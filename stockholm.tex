%% Hey, emacs(1), this is -*- mode: latex; coding: utf-8; fill-column: 79 -*-

%%
%% Example LaTeX source file for submissions to NoDaLiDa 20013; with very minor
%% adaptations from the COLING 2012 templates (adapted with permission from the
%% original authors, Christian Boitet and Gilles Sérasset).
%%
%% version 1.0; last revision: 23-sep-12 by Stephan Oepen (oe@ifi.uio.no)
%%

\documentclass[10pt,a5paper,twoside]{article}
\usepackage{nodalida13}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{relsize}


\usepackage{scrextend}
\deffootnote[2em]{2em}{1em}{\textsuperscript{\thefootnotemark}\,}

%TODO: FIND A TITLE THAT ACTUALLY MAKES SENSE. IDEAS??
\title{Linking articles to artists: offering researchers possible new research\\
     Temporary (and inapropriate title)}

%%
%% reviewing will be double-blind, i.e. submissions must be anonymous and not
%% otherwise reveal author identities (for example through self-citations)
%%
\iffalse
\author{%
  \textit{Mariano Mora-McGinity$^{1}$, György Fazekas$^{1}$}\\[0.8ex]
  {\small (1) Queen Mary University, London\\
          (2) Northampton Universtiy, Northampton\\[0.8ex]
  \texttt{m.mora-mcgintiy@qmul.ac.uk},
    \texttt{}}
\fi

\begin{document}

\maketitle

\abstract{%
The abstract should include Gary's work. So I won't write it now
}
\keywords{Semantic metadata, Research, Linking data}

%%
%% at this point (following the title page and abstract), submissions must
%% force a page break, such that the body of each paper starts on page two.
%%
\newpage


\section{Main Content of the Paper (in English)}
\label{sc:main}



\subsection{Methods}
\label{ss:methods}
The following section outlines the methods employed in the research project. The outline follows the three main concerns of the project's realization: 
how to \emph{acquire} data, how to \emph{link} data with data and how to \emph{visualize} data.

\subsubsection{Data acquisition}
\label{sss:acquisition}

The main objective of the project is to offer researchers a hub of information resources. These resources are presented in the form of linked data, thus connecting information which might be valuable to the researcher and which might have been missed otherwise. Our guiding principle has been to search for data and try to find criteria that would point to other related data. This process is of course cumulative, in two senses: a) the more data that we find the more new data that it can be linked to, and b) each new piece of data found is incorporated into the database of parameters that allow us to search for content in data, thus allowing us to identify data related to a subject.

Our main method of gathering data has been to query several services available on the web. Since researchers were mostly interested in music and its relation to other humanities and social science disciplines, our starting point has been to search information related to musicians. There are many web resources offering data about music, musicians, concerts, statistics about music listener tendencies, etc.\footnote{A complete listing of web resources on music can be found at: \\
  http://www.programmableweb.com/category/all/apis?keyword=music}. 
Our main source of information about artists was musicbrainz\footnote{https://musicbrainz.org/}, which includes an artist-id universally accepted by most
other web resources, and echonest\footnote{http://the.echonest.com/}. From these resources we have pulled the following information:
\begin{compactitem}
  \item 
    Artist summary
  \item
    Biography
  \item
    Blogs with content related to the artist
  \item
    List of similar artists
  \item 
    List of tags
\end{compactitem}

This information forms the first layer of knowledge about an artist. Knowledge about an artist will hopefully help to build a corpus which will help to identify articles and research publications about that artist automatically. To build this corpus we have selected and parsed a number of articles about each artist. The articles have been pulled also from web resources, mainly mendeley\footnote{http://apidocs.mendeley.com/home/public-resources} and core\footnote{http://core.kmi.open.ac.uk/search}. Each article selected contains:
\begin{compactitem}
  \item
    Type of publication: article, proceedings, book...
  \item 
    List of authors
  \item
    Abstract
  \item
    Publication in which the article appears
  \item
    Related articles
  \item 
    Disciplines
\end{compactitem}
Knowledge of the artist is further enhanced by adding the `disciplines' and `publications' categories. The rational behind this choice is that potential articles are probably published in similar journals, and tagged as belonging to similar disciplines.
The core of our semantic database is made up of these articles and the relations between them. 

\subsubsection{Data Linking}
\label{sss:linking}
%TODO: include bibliography about the music ontology
Our semantic database links artists with research related to them. The two main ontologies used to bind these two domains together are the \emph{Music Ontology}\footnote{http://musicontology.com/} and the bibliographic ontology \emph{fabio}\footnote{http://www.essepuntato.it/lode/http://purl.org/spar/fabio}. Graphs are built based on the artist. A first binding is established between each artist and all similar (according to the web resources) artists. A second level of binding is then established between each artist and all the articles available in our semantic database that are related to the artist. From each article the researcher can access information about related/similar articles, other articles from the same author, articles published in the same journal and articles that belong to the same disciplines, or are tagged in a similar way.   
Many of the classes and predicates can be found in the ontologies used. We created several classes and predicates to help bind relations.  Examples of these are:
\begin{compactitem}
  \item
    isAbout
  \item 
    isPublishedIn
  \item
    isSimilarTo
  \item
    isRelatedTo
  \item
    isSimilarlyTaggedAs
\end{compactitem}
Most of the predicates and classes used have been motivated by the desire to offer the user different approaches to navigating the data. The navigation itself has also been determined by the principle of presenting the data to the researcher in a clear and coherent way.

\subsubsection{Data Visualizing}
\label{sss:visualizing}

The main motivation while designing the data visualization interface is to facilitate the researcher's navigation through the data. There are many conceivable ways to enter a connected graph. Our intention has been to offer the researcher different ways to begin the search and different possibilities to continue at each step of the navigation. Figure~\ref{fg:coling} shows a tree as may be seen by a researcher. The user selects a search category and a search term. The initial data about this object is presented in a dynamic tree format. Each node in the tree contains sub-trees with possible data categories. An instance of a brach is, for instance, a new sub-tree of journals that publish articles about that artist. Opening that node uncovers new branches of disciplines included in that particular journal, as well as all nodes to all articles published in that journal, as well as all authors who have published in that journal.


\begin{figure}
  \centering
  \includegraphics[scale=0.70]{coling.jpg}
  \caption{View of the data visualization interface}
  \label{fg:coling}
\end{figure}

Another example of a sub-tree is that of a branch containing references to similar artists. Clicking on the node of a similar artist creates a new tree with the artist as a root. The same happens if the user selects a keyword or tag: a tree is built rooted on that keyword and showing all articles similarly tagged, together with the artists related to the articles, etc...
The user navigates the tree until coming on a leave, which also presents linked data, however not in the form of sub-trees. This data may link to external resources, as is the case in web-links, or to new data that becomes the root of a new main tree. 


\subsection{Future Work}
\label{ss:future}

Future work should focus on improving and refining the three levels of development mentioned in the methods sections. 

\subsubsection{Data Acquisition}
\label{sssf:acquisition}

The main thrust of future work should concentrate on developing ways to acquire new data. Once a big enough corpus of articles has been built it should be possible to discriminate articles using machine learning methods. Some methods which should be explored are:
\begin{compactitem}
  \item
    Clustering of articles using features such as journal, tags, discipline, author, etc.
  \item 
    Semantic analyis of article abstract. This should serve as a first filter to identify possible articles.
  \item
    Semantic analysis of the articles. Concept identification would increase the knowledge about artists and could enrich the researcher experience.
\end{compactitem}

\subsubsection{Data Linking}
\label{sssf:linking}

In the near future we expect to:
\begin{compactitem}
  \item
    Build an ontology which would allow the user to inference relations between artists and concepts.
  \item 
    A growing database of knowledge would also increase the semantic database of relations.
  \item
    Giving researchers the possibility of using and evaluating the resource would give us many chances of refining the conceptual ontology thay would be most interested in.
\end{compactitem}

\subsubsection{Data Visualizing}
\label{sssf:visualization}
We expect to make the resource accesible to reseachers in the following months. Their feedback would greatly determine our next choices regarding data visualization. We are planning evaluations by users of:

\begin{compactitem}
  \item
    Accessibility of the data. How many layers do they navigate before they find relevant data? Should the trees be arranged otherwise?
  \item
    Clarity of the data. Does the visualization aid their search for data, or does it hinder it? If so, how could it be improved?
  \item
    Level of engagement. Are they attracted to the resource by the way the data is presented? If not, how would things need to be changed?
\end{compactitem}


\newpage
















\section*{Acknowledgments (Unnumbered, Use Style Heading1)}
The name of the grant

\newpage
\bibliographystyle{apalike}
\bibliography{sample}


\end{document}
